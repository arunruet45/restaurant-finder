# Restaurant finder
## Objectives
1. Random selection of a restaurant within 3 km of the Monstarlab Bangladesh Office.
2. A map view showing the location of the restaurant.
3. Keyword search for restaurants within 3 km of the office

## Project Status
Necessary environment  `Done`

Feature Implementation  `Done`

Unit and Integration testing  `In Progress`

#### Project Screen Shot(s)
`https://drive.google.com/file/d/1bZQXYvC5dddV0I2MXqTUB5CIk2yKFDBD/view?usp=sharing`
#### Bitbucket link
`https://bitbucket.org/arunruet45/restaurant-finder`
## Installation and Setup Instructions
You will need `git`, `node`  and  `yarn`  installed globally on your machine.

Open terminal and run these commands
```
git clone https://arunruet45@bitbucket.org/arunruet45/restaurant-finder.git
cd restaurant-finder
yarn install
```
To Run on local machine

`yarn start`

To Run Test Suite

`yarn test`

To Visit App on local

`https://localhost:[port-no]`

To build for stg

`yarn build:stg`

To build for production

`yarn build:prod`

## Reflection
```
This is a project that allow user to find restaurants inside 3km area of MonstarLab BD office. 
User can see the name, type and distance from Monstarlab office to restaurant from google map 
and also from list that is present on sidebar.
There is a list of restaurants and user can search restaurants from list by name and
user can click any restaurents from list, google map will show marker only on that restaurant.
There is a button available for random search among restaurents. If user click this button an random 
restaurant will be selected, google map will show marker only on that restaurant.                            

The technologies implemented in this project are React, React, React-dom, react-router-dom v6, 
react-google-maps/api, React hooks, Context API, Typescript, antd, bootstrap, lodash, axios, foursquare api, jest, 
react testing library. 
```
## Technical specification

```
## Context-API
Used to to state management among components

## react-google-maps/api
Used this library to implement google map. This library is quite simple and quite more used 
among others choices. 

##Foursquare API
Used for getting actual place data

##Axios
Used for API calling. It's a very simple AJAX calling API

## Lodash
Used it because of its some usefull functions to use. 

## antd
It's a very popular react UI library. Because it's a react poject so antd used here to simplify the 
design related things 
```