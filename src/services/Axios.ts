import axios from 'axios';

const axiosInstance = axios.create({
    responseType: 'json',
    headers: {
        'Content-Type': 'application/json'
    }
});

export default axiosInstance;
