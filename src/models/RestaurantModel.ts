type categoriesType = [
    {
        name: string
    }
];

type location = {
    country: string
};

type geocodes = {
    main: geocode
};

type geocode = {
    latitude: number,
    longitude: number
}

export type ForSquarePlaceDataType = {
    location: location;
    categories: categoriesType;
    geocodes: geocodes;
    fsq_id: string
    categoryName: false,
    name: string,
    distance: number
    country: string
    timezone: string
};

export default class RestaurantModel {
    public categoryName: string;
    public name: string;
    public distance: number;
    public country: string;
    public timezone: string;
    public id: string;
    public geocode: geocode;

    constructor(options: ForSquarePlaceDataType) {
        options = options || {};

        this.id = options.fsq_id;
        this.categoryName = options.categories[0]?.name;
        this.name = options.name;
        this.distance = options.distance;
        this.geocode = options.geocodes.main;
        this.country = options.location.country;
        this.timezone = options.timezone;

    }
}