type DataType = {
    results: []
};

type SuccessParamsType = {
    data: DataType
};

export type ErrorParamsType = {
    message: object,
    data: DataType
};

const success = (response: SuccessParamsType) => {
    const {data} = response;
    return {
        success: true,
        data: data,
        error: []
    };
};

const error = (responseError: ErrorParamsType) => {
    const {message, data} = responseError;
    return {
        success: false,
        data: data,
        error: message,
    };
};

export {success, error};