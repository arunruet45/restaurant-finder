import {createContext} from 'react';

const initialState = {
    isAppReady: false,
    updateIsAppReady: (value: boolean) => {/**/
    },
    placesInfo: [],
    setPlacesToShowInMap: (value: object[]) => {/**/
    },
};

export const AppContext = createContext(initialState);

export default AppContext;