import axios from './Axios';
import {error, success} from './ResponseHandler';

const {REACT_APP_FOURSQUARE_API_URL, REACT_APP_FOURSQUARE_API_KEY, REACT_APP_FOURSQUARE_CLIENT_ID, REACT_APP_FOURSQUARE_CLIENT_SECRET} = process.env;
const GET_RESTAURANT_URL = REACT_APP_FOURSQUARE_API_URL + '/v3/places/search';

class MapServices {
    async getRestaurants(params: object) {
        try {
            params = {
                ...params,
                client_id: REACT_APP_FOURSQUARE_CLIENT_ID,
                client_secret: REACT_APP_FOURSQUARE_CLIENT_SECRET,
            };
            const response = await axios.get(GET_RESTAURANT_URL, {
                params: params, headers: {
                    Authorization: REACT_APP_FOURSQUARE_API_KEY || ''
                }
            });
            return success(response);

        } catch (err: any) {
            return error(err);
        }
    }
}

export default new MapServices();