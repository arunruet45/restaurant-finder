const {REACT_APP_ROUTE_BASE_PATH} = process.env;

export default {
    dashboard: REACT_APP_ROUTE_BASE_PATH + '/'
};