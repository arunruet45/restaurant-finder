import './RestaurantList.css';

import React, {useContext} from 'react';
import {List, Avatar} from 'antd';
import {_convertMeterToKM} from '../../../utils/Utils';
import AppContext from '../../../contexts/AppContext';
import RestaurantModel from '../../../models/RestaurantModel';

type RestaurantList = {
    restaurantsInfo: RestaurantModel[]
};

const RestaurantList = (props: RestaurantList) => {
    const {restaurantsInfo} = props;
    const {setPlacesToShowInMap} = useContext(AppContext);

    const onclickItem = (value: object) => () => {
        setPlacesToShowInMap([value]);
    };

    return (
        <List
            className="demo-loadmore-list -overflow"
            itemLayout="horizontal"
            dataSource={restaurantsInfo}
            renderItem={(item: RestaurantModel) => (
                <List.Item onClick={onclickItem(item)} className='cursor-pointer list'>
                    <List.Item.Meta
                        avatar={<Avatar/>}
                        title={<label className='cursor-pointer'>{item.name}</label>}
                        description={`${item.categoryName}, ${_convertMeterToKM(item.distance)}Km, ${item.country}, ${item.timezone}`}
                    />
                </List.Item>
            )}
        />
    );
};

export default RestaurantList;