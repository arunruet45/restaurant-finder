import React from "react";
import {render} from '@testing-library/react';
import RestaurantList from "../restaurant-list/RestaurantList";
import RestaurantModel from "../../../models/RestaurantModel";

it('should render a list', () => {
    const restaurantsInfo: RestaurantModel[] = [
        {
            id: 'Burger1',
            categoryName: 'Burger agent',
            name: 'Takeout',
            distance: 1000,
            geocode: {latitude: 10, longitude: 10},
            country: 'dhaka',
            timezone: 'Asia/Dhaka',
        }
    ];

    const {getAllByTestId} = render(<RestaurantList restaurantsInfo={restaurantsInfo}/>)
    const aa = getAllByTestId('restaurant').forEach((li)=> console.log(li) )
});