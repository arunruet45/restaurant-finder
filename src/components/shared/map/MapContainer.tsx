import React, {useContext, useState, Fragment, useEffect} from 'react';
import {GoogleMap, useJsApiLoader, Marker, InfoWindow} from '@react-google-maps/api';
import {_convertMeterToKM} from '../../../utils/Utils';
import CONSTANT from '../../../constants/Constant';
import AppContext from '../../../contexts/AppContext';
import RestaurantModel from '../../../models/RestaurantModel';

const {REACT_APP_GOOGLE_MAP_API_KEY} = process.env;

const MapContainer = () => {
    const {placesInfo} = useContext(AppContext);
    const [initialCenter, setInitialCenter] = useState(CONSTANT.GEOCODE.MONSTARLAB);
    const [focusedMarker, setFocusedMarker] = useState<RestaurantModel | null>(null);

    useEffect(() => {
        const place = placesInfo.find((item, index) => index === 0) ||
            {geocode: {latitude: CONSTANT.GEOCODE.MONSTARLAB.lat, longitude: CONSTANT.GEOCODE.MONSTARLAB.lng}};
        setInitialCenter({lat: place.geocode.latitude, lng: place.geocode.longitude});

    }, [placesInfo]);

    const {isLoaded} = useJsApiLoader({
        googleMapsApiKey: REACT_APP_GOOGLE_MAP_API_KEY || ''
    });

    const updateFocusedPlace = (place: RestaurantModel | null) => () => {
        setFocusedMarker(place);
    };

    return (
        <>
            {isLoaded && (
                <GoogleMap
                    mapContainerStyle={CONSTANT.MAP.CONTAINER_STYLE}
                    zoom={15}
                    center={initialCenter}>
                    {placesInfo.map((place: RestaurantModel) => {
                        return (
                            <Fragment key={place.id}>
                                <Marker position={{lat: place.geocode.latitude, lng: place.geocode.longitude}}
                                    onMouseOver={updateFocusedPlace(place)}
                                    onMouseOut={updateFocusedPlace(null)}>
                                </Marker>
                                {focusedMarker && (
                                    <InfoWindow
                                        position={{
                                            lat: focusedMarker.geocode.latitude,
                                            lng: focusedMarker.geocode.longitude
                                        }}
                                        onCloseClick={updateFocusedPlace(null)}>
                                        <div>
                                            <label>{focusedMarker.name}</label>
                                            <span>{`${focusedMarker.categoryName}, ${_convertMeterToKM(focusedMarker.distance)}Km, ${focusedMarker.country}, ${focusedMarker.timezone}`}</span>
                                        </div>
                                    </InfoWindow>
                                )}
                            </Fragment>
                        );
                    })}
                </GoogleMap>
            )}
        </>
    );
};

export default MapContainer;