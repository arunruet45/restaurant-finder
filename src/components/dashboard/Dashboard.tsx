import React, {useContext, useEffect, useState} from 'react';
import {Layout} from 'antd';
import {_sortBy} from '../../utils/Utils';
import AppContext from '../../contexts/AppContext';
import RestaurantModel, {ForSquarePlaceDataType} from '../../models/RestaurantModel';
import mapServices from '../../services/MapServices';
import Sidebar from '../shared/sidebar/Sidebar';
import MapContainer from '../shared/map/MapContainer';

const DashBoard = () => {
    const {updateIsAppReady} = useContext(AppContext);
    const [restaurantsInfo, setRestaurantsInfo] = useState<RestaurantModel[]>([]);

    useEffect(() => {
        // noinspection JSIgnoredPromiseFromCall
        getRestaurants();
    }, []);

    const getRestaurants = async () => {
        const params = {
            query: 'food',
            ll: '23.781718,90.400499',
            radius: 3000,
            limit: 50
        };

        const response = await mapServices.getRestaurants(params);
        const {data, success, error} = response;

        if (!success) {
            console.log(error);
            return;
        }

        const {results} = data;
        const restaurantList: RestaurantModel[] = [];
        results.forEach((item: ForSquarePlaceDataType) => {
            const restaurant = new RestaurantModel(item);
            restaurantList.push(restaurant);
        });

        const sortedListByDistance = _sortBy(restaurantList, 'distance');
        setRestaurantsInfo(sortedListByDistance);
        updateIsAppReady(true);
    };

    return (
        <Layout>
            <Sidebar restaurantsInfo={restaurantsInfo}/>
            <MapContainer/>
        </Layout>
    );
};

export default DashBoard;