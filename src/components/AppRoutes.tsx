import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import routes from '../constants/Routes';
import DashBoard from './dashboard/Dashboard';

export default (
    <BrowserRouter>
        <Routes>
            <Route path={routes.dashboard} element={<DashBoard/>}/>
        </Routes>
    </BrowserRouter>
);