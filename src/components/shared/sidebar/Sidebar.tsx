import './Sidebar.css';

import React, {useContext, useEffect, useState} from 'react';
import {Input, Layout, Button} from 'antd';
import debounce from 'lodash/debounce';
import RestaurantList from '../restaurant-list/RestaurantList';
import {_filterDataByKey, _getRandomItemFromArray} from '../../../utils/Utils';
import CONSTANT from '../../../constants/Constant';
import AppContext from '../../../contexts/AppContext';
import RestaurantModel from '../../../models/RestaurantModel';

const {Sider} = Layout;
type sidebarProps = {
    restaurantsInfo: RestaurantModel[]
}

const Sidebar = (props: sidebarProps) => {
    const {restaurantsInfo} = props;
    const {setPlacesToShowInMap} = useContext(AppContext);
    const [filteredRestaurantInfo, setFilteredRestaurantInfo] = useState(restaurantsInfo);
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        const filteredData = _filterDataByKey(restaurantsInfo, 'name', searchText);
        setFilteredRestaurantInfo(filteredData);
        setPlacesToShowInMap(filteredData);

    }, [restaurantsInfo]);

    const onChangeSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = (event.target.value).toLowerCase();
        setSearchText(value);
        const filteredData: RestaurantModel[] = _filterDataByKey(restaurantsInfo, 'name', value);
        setFilteredRestaurantInfo(filteredData);
        setPlacesToShowInMap(filteredData);
    };

    const onClickRandomSearch = () => {
        const randomItem= _getRandomItemFromArray(restaurantsInfo);
        setPlacesToShowInMap([randomItem]);
    };

    return (
        <>
            <Sider className="site-layout-background sidebar" breakpoint="lg" collapsedWidth="0">
                <div className="d-flex flex-column text-center action">
                    <label className='mt-2'>{CONSTANT.TEXT.TO_SELECT_RANDOM_RESTAURANT}</label>
                    <Button onClick={onClickRandomSearch} className="w-75 align-self-center" type="primary">
                        {CONSTANT.TEXT.CLICK_HERE}
                    </Button>
                    <label>{CONSTANT.TEXT.OR}</label>
                    <Input placeholder="Search restaurant here" className="w-75 align-self-center"
                        onChange={debounce(onChangeSearch, 500)}/>
                </div>
                <div className="sidebar-list d-flex flex-column w-100">
                    <div className='text-center'>
                        <label>{CONSTANT.TEXT.INSIDE_3KM_OF_MONSTARLAB_OFFICE}</label>
                    </div>
                    <div className='overflow-auto'>
                        <RestaurantList restaurantsInfo={filteredRestaurantInfo}/>
                    </div>
                </div>
            </Sider>
        </>
    );
};

export default Sidebar;