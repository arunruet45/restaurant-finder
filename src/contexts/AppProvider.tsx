import React, {Component} from 'react';
import AppContext from './AppContext';

class AppProvider extends Component<object, object> {
    state = {
        isAppReady: false,
        placesInfo: [],
    };

    updateIsAppReady = (value: boolean) => {
        this.setState({isAppReady: value});
    };

    setPlacesToShowInMap = (value: object[]) => {
        this.setState({placesInfo: value});
    };

    render() {
        const {children} = this.props;
        return (
            <AppContext.Provider value={{
                ...this.state,
                updateIsAppReady: this.updateIsAppReady,
                setPlacesToShowInMap: this.setPlacesToShowInMap,
            }}>
                {children}
            </AppContext.Provider>
        );
    }
}

export default AppProvider;