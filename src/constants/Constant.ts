const TEXT = Object.freeze({
    TO_SELECT_RANDOM_RESTAURANT: 'To select Random restaurant Inside 3km of Monstarlab office',
    CLICK_HERE: 'Click here',
    OR: 'Or',
    INSIDE_3KM_OF_MONSTARLAB_OFFICE: 'Inside 3km of Monstarlab office'
});

const GEOCODE = Object.freeze({
    MONSTARLAB: {lat: 23.781718, lng: 90.400499},
});

const MAP = Object.freeze({
    CONTAINER_STYLE: {
        width: '100vw',
        height: '100vh'
    }
});

const CONSTANT = {
    TEXT: TEXT,
    GEOCODE: GEOCODE,
    MAP: MAP
};

export default CONSTANT;