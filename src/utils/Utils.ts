export const _convertMeterToKM = (value: number) => {
    return (value / 1000).toFixed(1);
};

export const _filterDataByKey = (data: any[], key: string, searchText: string) => {
    return data.filter((item: Record<string, any>) => item[key]?.toLowerCase().trim().match(searchText.toLowerCase()));
};

export const _getRandomItemFromArray = (data: any[]) => {
    const index = Math.floor(Math.random() * data.length);
    return data[index];
};

export const _propComparator = (propertyName: string) => {
    return function (a: any, b: any) {
        return a[propertyName] > b[propertyName] ? 1 : -1;
    };
};

export const _sortBy = (arrayList: any[] = [], propertyName: string) => {
    return arrayList.sort(_propComparator(propertyName));
};